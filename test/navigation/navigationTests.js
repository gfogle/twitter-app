'use strict';

describe('NavigationController', function () {
    
    var createController,
        scope,
        $state,
        twitterService;
     
    beforeEach(function () {
        
        module('twitterApp');
        module('twitterApp.controllers');
        
        inject(function ($rootScope, $controller) {
        
            scope = $rootScope.$new();

            $controller('navigationController', {
                $scope: scope
            });

        });
    });
    
    it('loads controller', function () {
        
        dump(scope);
        expect(scope.currentView).toEqual('app.twitter');
        
    });
});
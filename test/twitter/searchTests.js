'use strict';

describe('SearchController', function () {
    
    var controller,
        scope,
        twitterService,
        events;
    
    beforeEach(module('twitterApp'));
    beforeEach(module('twitterApp.services'));
    
    beforeEach(inject(function ($controller, $rootScope) {
        
        scope = $rootScope.$new();
        controller = $controller('searchController', {
            $scope: scope
        });
    }));
    
    it('loads controller', function () {
        
        should.exist(controller);
    });
});
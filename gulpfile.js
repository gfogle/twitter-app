'use strict';

var gulp          = require('gulp'),
    browserSync   = require('browser-sync'),
    changed       = require('gulp-changed'),
    sass          = require('gulp-sass'),
    autoprefixer  = require('autoprefixer-core'),
    concat        = require('gulp-concat'),
    del           = require('del'),
    minifyCSS     = require('gulp-minify-css'),
    CSSlint       = require('gulp-csslint'),
    imagemin      = require('gulp-imagemin'),
    unCSS         = require('gulp-uncss'),
    glob          = require('glob'),
    postCSS       = require('gulp-postcss'),
    plumber       = require('gulp-plumber'),
    runSequence   = require('run-sequence'),
    karma         = require('karma').server;


var testFiles     = [
    'node_modules/angular/angular.js',
    'node_modules/angular-mocks/angular-mocks.js',
    './dist/**/*.js',
    './test/**/*.js'
];

/*
 *  Copy Index
 *
 *      1. Copy index.html file
 */
gulp.task('copy-index', function () {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('dist'));
});

/*
 *  Copy Favicon
 *
 */
gulp.task('copy-favicon', function () {
    return gulp.src('src/favicon.ico')
        .pipe(gulp.dest('dist'));
});

/*
 *  Copy Images
 *
 *      1. Copy any image files that get imported or changed
 */
gulp.task('copy-images', function () {
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
});

/*
 *  Copy Lib CSS
 *
 *      1. Copy any 3rd party CSS files that get imported or changed
 */
gulp.task('copy-lib-css', function () {
    return gulp.src('src/css/lib/**/*')
        //.pipe(minifyCSS())
        .pipe(gulp.dest('dist/css/lib'));
});

/*
 *  Copy Fonts
 *
 *      1. Copy any 3rd party fonts that get imported or changed
 */
gulp.task('copy-fonts', function () {
    return gulp.src('src/css/fonts/*.*')
        .pipe(gulp.dest('dist/css/fonts'));
});

/*
 *  Copy Lib JS
 *      
 *      1. Copy any 3rd party JS files that get imported or changed
 */
gulp.task('copy-lib-js', function () {
    return gulp.src('src/js/lib/**/*')
        .pipe(gulp.dest('dist/js/lib'));
});

/*
 *  Copy JS
 *      
 *      1. Copy all JS built for the app
 */
gulp.task('copy-app-js', function () {
    return gulp.src(['src/js/**/*.js', '!src/js/lib/**/*.js'])
        .pipe(gulp.dest('dist/js'));
});

/*
 *  Copy Views
 */
gulp.task('copy-views', function () {
    return gulp.src('src/views/**/*.html')
        .pipe(gulp.dest('dist/views'));
});

/*
 *  SASS
 *
 */
gulp.task('sass', function () {
    return gulp.src('src/css/sass/style.scss')
        .pipe(plumber())
        .pipe(sass(
            {
                errLogToConsole : true,
                outputStyle: 'nested'
            }
        ))
        .pipe(postCSS([autoprefixer({ browsers: ['last 25 versions'] })]))
        .pipe(CSSlint(
            {
                'adjoining-classes' : false, // SASS compiles to adjoining. an IE6 rule anyways
                'outline-none' : false, // We override default browser for this everywhere
                'compatible-vendor-prefixes' : false, // -moz tags aren't needed anymore; causing errors
                'box-sizing' : false, // IE6 and IE7 rules basically
                'ids' : false, // basically just doesn't like using IDs in selectors
                'fallback-colors' : false, // only old browsers dont support RGBA
                'font-sizes' : false // do we really need to bother worrying about font sizes?
            }
        ))
        .pipe(CSSlint.reporter())
        .pipe(minifyCSS())
        .pipe(gulp.dest('dist/css'));
});

/*
 * Force Reload of Browser Sync
 */
gulp.task('bs-reload', function () {
    browserSync.reload();
});

/*
 * Run tests configured in karma.conf.js
 */
gulp.task('test', function (done) {
    
    return karma.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done);
    
});
          
gulp.task(
    'default',
    [
        'copy-lib-css',
        'copy-lib-js',
        'copy-app-js',
        'copy-views',
        'copy-images',
        'copy-fonts',
        'copy-favicon',
        'copy-index',
        'sass'
    ],
    function () {
        
        /*
         *  Browser Sync
         *
         *      1. Starts server on localhost:8000
         *      2. Dont watch 'src' folder watch 'dist'
         */
        browserSync({
            port: 8000,
            host: "localhost",
            logLevel: "debug",
            injectChanges: true,
            ghostMode: false,
            server: {
                baseDir: "dist"
            }
        });
        
        gulp.watch("src/css/sass/*.scss", function () {
            
            runSequence('sass', 'bs-reload');
        });
        
        gulp.watch(
            "src/views/**/*.html",
            ['copy-views', 'bs-reload']
        );
        
        gulp.watch(
            ['src/js/**/*.js', '!src/js/lib/**/*.js'],
            ['copy-app-js', 'bs-reload']
        );
        
        gulp.watch(
            "src/img/*",
            ['copy-images', 'bs-reload']
        );
        
        gulp.watch(
            "src/index.html",
            ['copy-index', 'bs-reload']
        );
    }
);
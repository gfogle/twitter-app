twitter-app.git
=========

```sh
cd $HOME
sudo apt-get install -y git-core
git clone https://gfogle@bitbucket.org/gfogle/twitter-app.git
  
```

If you dont already have node installed, install
it before anything else:
```sh
sudo apt-get install npm
```

Sometimes, like on EC2, bower wont install 
on the post install command so run
```sh
bower install
```

File Structure
```
.
|---  src
|     |--  css
|          |--  fonts
|          |--  lib
|          |--  modules
|     |--  img
|     |--  js
|          |--  app.js
|          |--  controllers
|          |--  directives
|          |--  enums
|          |--  filters
|          |--  lib
|          |--  services
|          |--  strings
|     |--  views
|     |--  index.html
|     gulpfile.js
|     package.json
|     README.md
|     favicon.ico
|---  test
|---  dist

```
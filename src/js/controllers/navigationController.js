'use strict';

angular.module('twitterApp.controllers').controller(
    'navigationController',
    [
        '$scope',
        '$state',
        'events',
        function ($scope, $state, events) {
            
            (function () {

                // default state is search
                if (!$state.current.name) {
                    
                    $scope.currentView = 'app.twitter';
                }
            }());
            
            $scope.$on(
                '$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    
                    $scope.currentView = toState.name;
                }
            );
            
        
            $scope.setSubView = function (subView) {
                
                $scope.currentSubView = subView;
                
                if (subView === 'search-twitter') {
                    
                    amplify.publish(events.navigation.SEARCH_TWITTER);
                }
                
                if (subView === 'twitter-timeline') {

                    amplify.publish(events.navigation.TWITTER_TIMELINE);
                }
            };
        }
    ]
);
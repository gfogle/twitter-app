'use strict';

angular.module('twitterApp.controllers').controller(
    'twitterController',
    [
        '$scope',
        'twitterService',
        'events',
        function ($scope, twitterService, events) {
            
            $scope.$on(
                '$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    
                    $scope.currentView = null;
                    $scope.currentView = null;
                }
            );
            
            $scope.searchResult = {};
            $scope.currentView = null;
            $scope.isConnected = false;
            $scope.timeline = [];
            
            /*
             * Use twitter service to create a popup that will authenticate
             */
            $scope.connect = function () {
                
                twitterService.connect().then(function () {
                    
                    console.log("connect returned");
                    
                    if (twitterService.isConnected()) {
                        
                        console.dir(twitterService.isConnected());
                        
                        $scope.isConnected  = true;
                        twitterService.getTimeline().then(function (data) {
                            
                            $scope.timeline = data;
                            
                        });
                        
                    }
                    
                });
            };
            
            /*
             * @param phrase - the phrase to search twitter by
             */
            $scope.searchTwitter = function (phrase) {
                
                twitterService.searchTweets(phrase)
                    .then(function (results) {
                        
                        $scope.searchResult.tweetsFound = results.statuses;

                    });
            };
            
            /*
             * @param status - the status to update
             */
            $scope.postTwitter = function (status) {
                
                twitterService.postStatus(status)
                    .then(function (results) {
                        
                        //$scope.searchResult.tweetsFound = results.statuses;
                        console.dir(results);
                    });
            };
            
            /*
             *  This event gets published when the active page in navigation
             *  is twitter and a user clicks on the search twitter section in
             *  the sub menu
             */
            amplify.subscribe(events.navigation.SEARCH_TWITTER, function () {
                
                $scope.currentView = 'search-twitter';
            });
            
            /*
             *  This event gets published when the active page in navigation
             *  is twitter and a user clicks on the twitter timeline section in
             *  the sub menu
             */
            amplify.subscribe(events.navigation.TWITTER_TIMELINE, function () {
                
                $scope.currentView = 'twitter-timeline';
            });
        }
    ]
);
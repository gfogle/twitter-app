'use strict';

angular.module('twitterApp.services').service('events', [ function () {

    return {

        navigation: {
            SEARCH_TWITTER:   'search-twitter',
            TWITTER_TIMELINE: 'twitter-timeline'
        },

        twitter: {
            CONNECT_SUCCESS: 'connect-success'
        }
    };

}]);
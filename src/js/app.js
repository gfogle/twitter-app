'use strict';

angular.module(
    'twitterApp',
    [
        'twitterApp.filters',
        'twitterApp.services',
        'twitterApp.directives',
        'twitterApp.controllers',
        'ui.router',
        'ngResource',
        'angularMoment'
    ]
).config(function ($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/twitter');
    
    var ABSTRACT_TEMPLATE = '<ui-view/>';
    
    $stateProvider
        .state('app.twitter', {
            url: '/twitter',
            templateUrl: 'views/twitter.html',
            controller: 'twitterController'
        })
        .state('app.analytics', {
            url: '/analytics',
            templateUrl: 'views/analytics.html'
        })
        .state('app.stack', {
            url: '/stack',
            templateUrl: 'views/stack.html'
        })
        .state('app', {
            abstract:       true,
            template:       ABSTRACT_TEMPLATE
        });
});

angular.module('twitterApp.controllers', []);
angular.module('twitterApp.services', []);
angular.module('twitterApp.directives', []);
angular.module('twitterApp.filters', []);
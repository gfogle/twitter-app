'use strict';

angular.module('twitterApp.services').service(
    'twitterService',
    [
        '$q',
        '$http',
        'events',
        function (
            $q,
            $http,
            events
        ) {
                
            var oAuthResult = false,
                service = {
                    
                    /*
                     * Creates popup to authenticate with twitter
                     *
                     *
                     * @returns {promise|Q.promise|Tc.g.promise}
                    */
                    connect: function () {
                        
                        var deferred = $q.defer();
                        
                        OAuth.popup('twitter', {cache : false}, function (error, result) {
                            
                            if (!error) {
                                
                                oAuthResult = result;
                                
                                deferred.resolve();
                                
                            } else {
                                
                                console.dir(error);
                                
                            }
                        });
                        
                        return deferred.promise;
                    },
                    
                    /*
                     * Gets a users timeline
                     */
                    getTimeline: function () {
                        
                        var deferred = $q.defer(),
                            promise = oAuthResult.get('/1.1/statuses/home_timeline.json').done(function (data) {
                                
                                deferred.resolve(data);
                            });
                                                                      
                        return deferred.promise;
                    },
                    
                    /*
                     * returns false if connect method hasnt been called
                     */
                    isConnected: function () {
                        
                        return (oAuthResult);
                    },
                    
                    /*
                     * posts a status to twitter
                     */
                    postStatus: function (status) {
                        
                        var deferred = $q.defer(),
                            promise = oAuthResult.post({
                                url: '/1.1/statuses/update.json',
                                data: { status: status }
                            }).done(function (data) {
                                
                                deferred.resolve(data);
                            });
                                                                      
                        return deferred.promise;
                    },
                    
                    /*
                     * Searches twitter for a given phrase
                     *
                     * @param phrase    - phrase to search for
                     *
                     * @returns {promise|Q.promise|Tc.g.promise}
                    */
                    searchTweets: function (phrase) {
                        
                        var deferred = $q.defer(),
                            promise = oAuthResult.get('/1.1/search/tweets.json?q=' + phrase)
                                .done(function (data) {
                                
                                    deferred.resolve(data);
                                });
                                                                      
                        return deferred.promise;
                    }
                    
                };
            
            // TODO: move all of this to API? ... might be odd with the redirect
            (function () {
                OAuth.initialize('zrMNhtfHQ9ChZK34iT0gTELHwy0', {cache : false});
                
                oAuthResult = OAuth.create('twitter');
            }());
                
            return service;
        }
    ]
);